# workflow-tampermonkey

1. Öffne Tampermonkey
2. Gehe zu `Hilfsmittel`
3. Url einfügen: `https://gitlab.com/d_ani/workflow-tampermonkey/-/raw/release/dist/user.js?inline=false`
4. `Installieren` klicken

![imgs/step1.png](imgs/step1.png)

Und anschließend nochmal `Installieren` drücken:

![imgs/step2.png](imgs/step2.png)
