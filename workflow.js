// ==UserScript==
// @name         Workflow Tag
// @namespace    https://bierzappfer.de
// @version      0.1
// @description  Add additonal information in workflow
// @author       Bierzappfer
// @match        https://*/workflow/Projektzeit/edit.asp*
// @grant        none
// @downloadURL  https://gitlab.com/d_ani/workflow-tampermonkey/-/raw/release/dist/user.js?inline=false
// @updateURL    https://gitlab.com/d_ani/workflow-tampermonkey/-/raw/release/dist/meta.js?inline=false
// ==/UserScript==

function getTimeOfDayFullMinutes(t) {
    return (t.getHours() * 60 + t.getMinutes()) * 60 * 1000;
}

function parseTime(t) {
    let d = new Date();
    let time = t.match(/(\d+)(?::(\d\d))?\s*(p?)/);
    d.setHours(parseInt(time[1]) + (time[3] ? 12 : 0));
    d.setMinutes(parseInt(time[2]) || 0);
    return getTimeOfDayFullMinutes(d);
}

function getDate(t) {
    let copy = new Date(t);
    copy.setHours(0);
    copy.setMinutes(0);
    copy.setSeconds(0);
    copy.setMilliseconds(0);
    return copy;
}

function getCurrentTimeOfDay() {
    let now = new Date();
    return getTimeOfDayFullMinutes(now);
}

function toTimeString(t) {
    if (t === undefined || t === null) {
        return null;
    }
    let sign = t < 0 ? '-' : '';
    t = Math.abs(t);
    let hours = Math.floor(t / 3600.0 / 1000);
    let minutes = Math.round(t / 3600.0 / 1000 * 60 % 60);
    return sign + hours + ':' + (minutes < 10 ? '0' : '') + minutes;
}

// Evaluate an XPath expression aExpression against a given DOM node
// or Document object (aNode), returning the results as an array
// thanks wanderingstan at morethanwarm dot mail dot com for the
// initial work.
// source: https://developer.mozilla.org/en-US/docs/Web/XPath/Snippets
function evaluateXPath(aNode, aExpr) {
    let xpe = new XPathEvaluator();
    let nsResolver = xpe.createNSResolver(aNode.ownerDocument == null ?
        aNode.documentElement : aNode.ownerDocument.documentElement);
    let result = xpe.evaluate(aExpr, aNode, nsResolver, XPathResult.ANY_TYPE, null);
    let found = [];
    let res;
    while (res = result.iterateNext())
        found.push(res);
    return found;
}

function getBookingsTable() {
    try {
        return evaluateXPath(document, '//table[.//tr/td/text() = "Kommen" or .//tr/td/text() = "Gehen"]')[0];
    } catch {
        return null;
    }
}

function getSelectedDate() {
    try {
        let dateStr = evaluateXPath(document, '//td[.//input/@id = "ORIGINALDATUM"]//input[@id = "TTMMJJJJ"]')[0].value;
        let m = dateStr.match(/(\d+)\.(\d+)\.(\d+)/)
        return new Date(m[3] + '-' + m[2] + '-' + m[1]);
    } catch {
        return null;
    }
}

function getBookings() {
    try {
        let bookings = [];

        let rows = evaluateXPath(getBookingsTable(), './/tr');
        if (rows.length < 2) {
            return null;
        }

        let headerRow = rows[0];
        let timeRow = rows[1];
        let headers = evaluateXPath(headerRow, './td').map(td => td.innerText);
        let times = evaluateXPath(timeRow, './td').map(td => td.innerText);
        if (headers.length !== times.length) {
            return null;
        }

        for (let i = 0; i < headers.length; ++i) {
            bookings.push({
                type: headers[i],
                timeStr: times[i],
                time: parseTime(times[i]),
            });
        }

        return bookings;
    } catch {
        return null;
    }
}

let row1 = null;
let row1cell1 = null;
let row1cell2 = null;
let row2 = null;
let row2cell1 = null;
let row2cell2 = null;
let row3 = null;
let row3cell1 = null;
let row3cell2 = null;
let row4 = null;
let row4cell1 = null;
let row4cell2 = null;

function update() {
    'use strict';
    let bookings = getBookings();
    let selectedDate = getSelectedDate();
    if (!bookings || !selectedDate) {
        return;
    }
    let currentDay = getDate(selectedDate).getTime() === getDate(new Date()).getTime();
    console.log('currentDay', currentDay);

    let currentWorkTime = 0/*ms*/;
    let currentPauseTime = 0/*ms*/;
    let startTime = null;
    let state = null;
    bookings.forEach(b => {
        switch (state) {
            case 'Work':
                if (b.type === 'Gehen') {
                    if (b.time > startTime) {
                        currentWorkTime += b.time - startTime;
                    } else {
                        // might be the next day
                        currentWorkTime += b.time - startTime + (24 * 3600 * 1000/*ms*/);
                    }
                    startTime = b.time;
                    state = 'Pause';
                }
                break;

            case 'Pause':
                if (b.type === 'Kommen') {
                    if (b.time > startTime) {
                        currentPauseTime += b.time - startTime;
                    } else {
                        // might be the next day
                        currentPauseTime += b.time - startTime + (24 * 3600 * 1000/*ms*/);
                    }
                    startTime = b.time;
                    state = 'Work';
                }
                break;

            default:
                if (b.type === 'Kommen') {
                    startTime = b.time;
                    state = 'Work';
                }
                break;
        }
    });

    let additionalPauseTime = 0/*ms*/;
    if (currentDay) {
        switch (state) {
            case 'Work':
                currentWorkTime += getCurrentTimeOfDay() - startTime;
                break;

            case 'Pause':
                additionalPauseTime += getCurrentTimeOfDay() - startTime;
                break;

            default:
                // there was no booking today
                break;
        }
    }

    let nominalWorkTime = 8 * 3600 * 1000/*ms*/;
    let nominalPauseTime = 0/*ms*/;
    let predictedWorkTime = Math.max(currentWorkTime, nominalWorkTime)/*ms*/;
    if (predictedWorkTime > 9 * 3600 * 1000/*ms*/) {
        nominalPauseTime = 0.75 * 3600 * 1000/*ms*/;
    } else if (predictedWorkTime > 6 * 3600 * 1000/*ms*/) {
        nominalPauseTime = 0.5 * 3600 * 1000/*ms*/;
    }

    let remainingWorkTime = Math.max(0, nominalWorkTime - currentWorkTime)/*ms*/;
    let remainingPauseTime = Math.max(0, nominalPauseTime - currentPauseTime)/*ms*/;
    let needPause = remainingPauseTime > 0;
    // if (currentDay) {
    //     remainingPauseTime -= additionalPauseTime;
    // }

    let leaveTime = null;
    if (currentDay) {
        if (remainingWorkTime > 0) {
            leaveTime = getCurrentTimeOfDay() + remainingWorkTime + remainingPauseTime;
        } else {
            switch (state) {
                case 'Work':
                    leaveTime = getCurrentTimeOfDay() + nominalWorkTime - currentWorkTime;
                    break;

                case 'Pause':
                    leaveTime = null;
                    break;

                default:
                    // there was no booking today
                    break;
            }
        }
    }

    let table = getBookingsTable();
    if (needPause) {
        row1 = row1 || table.insertRow();
        row1.style.backgroundColor = "#FED200";
        row1cell1 = row1cell1 || row1.insertCell();
        row1cell1.innerHTML = "Pause noch zu machen:";
        row1cell2 = row1cell2 || row1.insertCell();
        row1cell2.colSpan = 1000;
        row1cell2.innerHTML = toTimeString(remainingPauseTime) + 'h';
    }

    if (leaveTime) {
        row2 = row2 || table.insertRow();
        row2.style.backgroundColor = "#FED200";
        row2cell1 = row2cell1 || row2.insertCell();
        row2cell1.innerHTML = "Regelarbeitszeit " + toTimeString(nominalWorkTime) + " Gehen:";
        row2cell2 = row2cell2 || row2.insertCell();
        row2cell2.colSpan = 1000;
        row2cell2.innerHTML = toTimeString(leaveTime) + ' Uhr';
    }

    row3 = row3 || table.insertRow();
    row3.style.backgroundColor = "#FED200";
    row3cell1 = row3cell1 || row3.insertCell();
    row3cell1.innerHTML = "Gesamtarbeitszeit:";
    row3cell2 = row3cell2 || row3.insertCell();
    row3cell2.colSpan = 1000;
    row3cell2.innerHTML = toTimeString(currentWorkTime) + 'h';

    row4 = row4 || table.insertRow();
    row4.style.backgroundColor = "#FED200";
    row4cell1 = row4cell1 || row4.insertCell();
    row4cell1.innerHTML = "Gesamtpausenzeit:";
    row4cell2 = row4cell2 || row4.insertCell();
    row4cell2.colSpan = 1000;
    row4cell2.innerHTML = toTimeString(currentPauseTime) + 'h';

}

setInterval(update, 300);

setTimeout(function () {
    let arbeitsfloge = document.getElementById('select_afo');
    let i;
    if (arbeitsfloge) {
        for (i = 0; i < arbeitsfloge.length; ++i) {
            if (arbeitsfloge.options[i].value == "120") {
                arbeitsfloge.value = 120;
            }
        }
    }
}, 2000); //Two seconds will elapse and Code will execute.
